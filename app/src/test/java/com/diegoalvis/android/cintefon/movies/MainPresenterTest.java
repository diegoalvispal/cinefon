package com.diegoalvis.android.cintefon.movies;

import com.diegoalvis.android.cintefon.interfaces.MainInterface;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.presenters.MainPresenter;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link MainPresenterTest}
 */
public class MainPresenterTest {

    private static List<MovieItem> MOVIES = Lists.newArrayList(new MovieItem());
//    private static List<MovieItem> EMPTY_MOVIES = Lists.newArrayList();

    @Mock
    private MainInterface mMainView;


    private MainPresenter mMainPresenter;

    @Before
    public void setupMainPresenter() {
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mMainPresenter = new MainPresenter(mMainView);

    }

    @Test
    public void loadMoviesFromServerAndLoadIntoView() {

        // Get list of movies
        mMainPresenter.getListMoviesFromService("popular", true);
        // verify progress is visible
        verify(mMainView).showProgress(true);

        // set movies in list
        mMainPresenter.showList(MOVIES);
        // verify adapter created, list created, progress is not visible and show list
        verify(mMainView).createMovieAdapter(MOVIES);
        verify(mMainView).createList(mMainView.createMovieAdapter(MOVIES));
        verify(mMainView).showProgress(false);
        verify(mMainView).showList();

        // get list when there is not internet access
        mMainPresenter.getListMoviesFromService("popular", false);
        // verify list hiden
        verify(mMainView).hideList();
    }

    @Test
    public void clickOnMovie_ShowsDetailUi() {
        MovieItem movieItem = new MovieItem();

        // select a movie
        mMainPresenter.onMovieSelect(movieItem);
        // verify show detail movie view
        verify(mMainView).showDetailUi(movieItem);
    }


}
