package com.diegoalvis.android.cintefon.movies;

import com.diegoalvis.android.cintefon.interfaces.DetInterface;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.presenters.DetailPresenter;
import com.google.common.collect.Lists;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.verify;

/**
 * Unit tests for the implementation of {@link DetailPresenterTest}
 */
public class DetailPresenterTest {

    private static List<MovieItem> MOVIES = Lists.newArrayList(new MovieItem());
//    private static List<MovieItem> EMPTY_MOVIES = Lists.newArrayList();

    @Mock
    private DetInterface mDetailView;


    private DetailPresenter mDetailPresenter;

    @Before
    public void setupMainPresenter() {
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this);

        // Get a reference to the class under test
        mDetailPresenter = new DetailPresenter(mDetailView);
    }

    @Test
    public void loadViews() {
        MovieItem movieItem = new MovieItem();

        // Set movie
        mDetailPresenter.getMovieData(movieItem);
        // Binnding data
        verify(mDetailView).bindingData(movieItem);
    }


}
