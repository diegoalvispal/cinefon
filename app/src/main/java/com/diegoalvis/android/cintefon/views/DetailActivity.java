package com.diegoalvis.android.cintefon.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;

import com.diegoalvis.android.cintefon.R;
import com.diegoalvis.android.cintefon.databinding.ActivityDetailBinding;
import com.diegoalvis.android.cintefon.interfaces.DetInterface;
import com.diegoalvis.android.cintefon.interfaces.DetPresenterInterface;
import com.diegoalvis.android.cintefon.models.MovieDetail;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.presenters.DetailPresenter;
import com.google.gson.Gson;

public class DetailActivity extends AppCompatActivity implements DetInterface {

    DetPresenterInterface presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        presenter = new DetailPresenter(this);
        presenter.getMovieData(new Gson().fromJson(getIntent().getStringExtra("movie"), MovieItem.class));
    }

    @Override
    public void bindingData(MovieItem movieItem) {
        MovieDetail movieDetail = new MovieDetail(
                movieItem.getVote_count(),
                movieItem.getVote_average(),
                movieItem.getPoster_path(),
                movieItem.getOverview(),
                movieItem.getRelease_date().split("-")[0],
                movieItem.getOriginal_title()
        );

        ActivityDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.setMovie(movieDetail);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        presenter.loadImageMovie(this, (ImageView) findViewById(R.id.iv_movie_detail), movieDetail.getPoster_path());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
