package com.diegoalvis.android.cintefon.interfaces;

import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.views.adapters.MovieAdapter;

import java.util.List;

import io.realm.Realm;


public interface MainInterface {

    void createList(MovieAdapter adapter);
    MovieAdapter createMovieAdapter(List<MovieItem> movies);
    void searchMovies(String keyWord);

    void errorLoad(String snackMsg, String actionMsg);

    void noItems();

    void showProgress(boolean isLoading);

    Realm initRealm();

    void hideList();
    void showList();

    void showDetailUi(MovieItem movieItem);

    void onMovieClick(MovieItem movieItem);
}
