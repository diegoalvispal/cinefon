package com.diegoalvis.android.cintefon.interfaces;


import android.view.Menu;
import android.view.View;

import com.diegoalvis.android.cintefon.models.MovieItem;

import java.util.List;

public interface MainPresenterInterface {

    void getListMoviesFromService(String category, boolean isInternetAvailable);

    boolean setMenu(Menu menu);

    String categorySelected(View view);
    //void onItemClicked(int position);

    void onMovieSelect(MovieItem movieItem);

}

