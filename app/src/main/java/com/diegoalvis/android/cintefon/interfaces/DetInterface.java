package com.diegoalvis.android.cintefon.interfaces;

import com.diegoalvis.android.cintefon.models.MovieItem;


public interface DetInterface {

    void bindingData(MovieItem movieDetail);

}
