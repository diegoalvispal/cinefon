package com.diegoalvis.android.cintefon.presenters;

import android.content.Context;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.diegoalvis.android.cintefon.interfaces.DetInterface;
import com.diegoalvis.android.cintefon.interfaces.DetPresenterInterface;
import com.diegoalvis.android.cintefon.models.MovieDetail;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.views.adapters.MovieAdapter;
import com.squareup.picasso.Picasso;

import io.realm.Realm;

/**
 * Created by diegoalvis on 3/18/17.
 */

public class DetailPresenter  implements DetPresenterInterface {

    DetInterface viewInterface;

    public DetailPresenter(DetInterface viewInterface) {
        this.viewInterface = viewInterface;
    }

    @Override
    public void getMovieData(MovieItem movieItem) {
        viewInterface.bindingData(movieItem);
    }

    @Override
    public void loadImageMovie(Context context, ImageView view, String poster_path) {
        try {
            Picasso.with(context).load(MovieAdapter.base_image_path + poster_path).into(view);
        } catch (Exception e) {
            Log.e("ALVIS", "Error to load image");
        }
    }


}
