package com.diegoalvis.android.cintefon.interfaces;


import android.content.Context;
import android.widget.ImageView;

import com.diegoalvis.android.cintefon.models.MovieItem;

public interface DetPresenterInterface {

    void getMovieData(MovieItem movieItem);
    void loadImageMovie(Context context, ImageView view, String poster_path);


}
