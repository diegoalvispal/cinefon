package com.diegoalvis.android.cintefon.presenters;

import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.diegoalvis.android.cintefon.R;
import com.diegoalvis.android.cintefon.interfaces.MainInterface;
import com.diegoalvis.android.cintefon.interfaces.MainPresenterInterface;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.networking.connection.ApiClient;
import com.diegoalvis.android.cintefon.networking.connection.ApiInterface;
import com.diegoalvis.android.cintefon.networking.responses.MovieItemResponse;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by diegoalvis on 3/15/17.
 */

public class MainPresenter implements MainPresenterInterface, SearchView.OnQueryTextListener {

    public static final String category_popular = "popular";
    public static final String category_upcoming = "upcoming";
    public static final String category_top_rated = "top_rated";

    String category;
    MainInterface viewInterface;
    Realm realm;

    public MainPresenter(MainInterface viewInterface) {
        this.viewInterface = viewInterface;
        this.realm = viewInterface.initRealm();
    }

    @Override
    public void getListMoviesFromService(String category, boolean isInternetAvailable) {
        this.category = (category != null) ? category : this.category;
        viewInterface.showProgress(true);
        if (isInternetAvailable)
            makeRequest();
        else {
            viewInterface.errorLoad("No internet access", "RETRY");
            viewInterface.showProgress(false);
            loadStoredData();
        }
    }

    public void showList(List<MovieItem> movies) {
        viewInterface.showList();
        viewInterface.showProgress(false);
        viewInterface.createList(viewInterface.createMovieAdapter(movies));
    }

    @Override
    public String categorySelected(View view) {
        String option = null;
        switch (view.getId()) {
            case R.id.op_popular:
                option = category_popular;
                break;
            case R.id.op_upcoming:
                option = category_upcoming;
                break;
            case R.id.op_top_rated:
                option = category_top_rated;
                break;
        }
        return option;
    }

    @Override
    public void onMovieSelect(MovieItem movieItem) {
        viewInterface.showDetailUi(movieItem);
    }

    //region Get Movies Data
    private void makeRequest() {
        ApiInterface apiInterface = ApiClient.getClient().create(ApiInterface.class);
        Call<MovieItemResponse> call = apiInterface.getMovies(category);
        call.enqueue(new Callback<MovieItemResponse>() {
            @Override
            public void onResponse(Call<MovieItemResponse> call, Response<MovieItemResponse> response) {
                viewInterface.showProgress(false);
                if(response.body().getResults().size() == 0) {
                    viewInterface.noItems();
                } else {
                    showList(response.body().getResults());
                    storeData(response.body().getResults());
                }
            }

            @Override
            public void onFailure(Call<MovieItemResponse> call, Throwable t) {
                viewInterface.showProgress(false);
                viewInterface.hideList();
                viewInterface.errorLoad("There was a problem loading movies", "RELOAD");
                Log.e("Alvis -- request", "Error getting movies list");
            }
        });
    }
    //endregion

    // region Data Base access write and read
    private void storeData(List<MovieItem> results) {
        realm.beginTransaction();
        for (int i = 0; i < results.size(); i++) {
            results.get(i).setCategory(category);
        }
        realm.copyToRealmOrUpdate(results);
        realm.commitTransaction();
    }

    private void loadStoredData() {
        this.realm = viewInterface.initRealm();
        try {
            List<MovieItem> results = realm.where(MovieItem.class).equalTo("category", category).findAll();
            if (results.size() > 0)
                showList(results);
            else
                viewInterface.hideList();
        } catch (Exception e) {
            viewInterface.hideList();
        }

    }
    // endregion


    // region search view
    @Override
    public boolean setMenu(Menu menu) {
        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        viewInterface.searchMovies(newText);
        return false;
    }
    // endregion


}
