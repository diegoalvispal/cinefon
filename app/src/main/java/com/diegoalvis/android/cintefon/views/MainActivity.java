package com.diegoalvis.android.cintefon.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.View;
import android.widget.RelativeLayout;

import com.diegoalvis.android.cintefon.R;
import com.diegoalvis.android.cintefon.helpers.ConnectionHelper;
import com.diegoalvis.android.cintefon.helpers.ResultsManager;
import com.diegoalvis.android.cintefon.interfaces.MainInterface;
import com.diegoalvis.android.cintefon.interfaces.MainPresenterInterface;
import com.diegoalvis.android.cintefon.interfaces.ResultManagerInterface;
import com.diegoalvis.android.cintefon.models.MovieItem;
import com.diegoalvis.android.cintefon.presenters.MainPresenter;
import com.diegoalvis.android.cintefon.views.adapters.MovieAdapter;
import com.google.gson.Gson;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;


public class MainActivity extends AppCompatActivity implements MainInterface, SwipeRefreshLayout.OnRefreshListener, ResultManagerInterface {

    RecyclerView recyclerView;
    SwipeRefreshLayout swipeRefresh;
    MainPresenterInterface presenter;

    public ResultsManager resultsManager = ResultsManager.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Realm.init(this);
        getViews();

        resultsManager.init(this, (RelativeLayout) findViewById(R.id.activity_main), this);
        presenter = new MainPresenter(this);
        optionClickCategory(findViewById(R.id.op_popular));
    }

    public void optionClickCategory(View view) {
        presenter.getListMoviesFromService(presenter.categorySelected(view), ConnectionHelper.isInternetAvailable(this));
    }


    @Override
    public MovieAdapter createMovieAdapter(List<MovieItem> movies) {
        return new MovieAdapter(this, movies, this);
    }

    @Override
    public void createList(MovieAdapter adapter) {
        recyclerView.setAdapter(adapter);
    }


    @Override
    public void searchMovies(String keyWord) {
        ((MovieAdapter)recyclerView.getAdapter()).searchResults(keyWord);
    }

    @Override
    public void errorLoad(String snackMsg, String actionMsg) {
        resultsManager.errorLoad(snackMsg, actionMsg);
    }

    @Override
    public void noItems() {
        resultsManager.noItems();
    }


    @Override
    public void showProgress(boolean isLoading) {
        resultsManager.showLoad(isLoading);
    }

    @Override
    public Realm initRealm() {
        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
        return Realm.getDefaultInstance();
    }

    @Override
    public void hideList() {
        recyclerView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showList() {
        recyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDetailUi(MovieItem movieItem) {
        Intent intent = new Intent(this, DetailActivity.class);
        intent.putExtra("movie", new Gson().toJson(movieItem));
        startActivity(intent);
    }

    @Override
    public void onMovieClick(MovieItem movieItem) {
        presenter.onMovieSelect(movieItem);
    }

    @Override
    public void actionSnack() {
        presenter.getListMoviesFromService(null, ConnectionHelper.isInternetAvailable(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return presenter.setMenu(menu);
    }

    // region get views from layout
    public void getViews() {
        GridLayoutManager glm = new GridLayoutManager(this, 2);
        glm.setOrientation(GridLayoutManager.VERTICAL);
        recyclerView = (RecyclerView) findViewById(R.id.rv_list_movies);
        recyclerView.setLayoutManager(glm);
        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_main);
        swipeRefresh.setOnRefreshListener(this);

    }

    @Override
    public void onRefresh() {
        presenter.getListMoviesFromService(null, ConnectionHelper.isInternetAvailable(this));
        swipeRefresh.setRefreshing(false);
    }
    // endregion
}
