package com.diegoalvis.android.cintefon.movies;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.action.ViewActions;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;

import com.diegoalvis.android.cintefon.R;
import com.diegoalvis.android.cintefon.views.DetailActivity;
import com.diegoalvis.android.cintefon.views.MainActivity;
import com.google.gson.Gson;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerMatchers.isClosed;
import static android.support.test.espresso.contrib.RecyclerViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDescendantOfA;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.google.common.base.Preconditions.checkArgument;
import static org.hamcrest.Matchers.allOf;

/**
 * Tests for the notes screen, the main screen which contains a grid of all notes.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class DetailScreenTest {

    static String movieStr = "{'poster_path':'/45Y1G5FEgttPAwjTYic6czC9xCn.jpg','adult':false,'overview':'Inthenearfuture,awearyLogancaresforanailingProfessorXinahideoutontheMexicanborder.ButLogan`sattemptstohidefromtheworldandhislegacyareup-endedwhenayoungmutantarrives,beingpursuedbydarkforces.','release_date':'2017-02-28','genre_ids':[28,18,878],'id':263115,'original_title':'Logan','original_language':'en','title':'Logan','backdrop_path':'/5pAGnkFYSsFJ99ZxDIYnhQbQFXs.jpg','popularity':151.093619,'vote_count':1528,'video':false,'vote_average':7.7}";

    private static String titleMovie = "Logan";
    private static String averageMovie = "7.7";
    private static String voteCountMovie = "Votes 1528";
    private static String overviewMovie = "Inthenearfuture,awearyLogancaresforanailingProfessorXinahideoutontheMexicanborder.ButLogan`sattemptstohidefromtheworldandhislegacyareup-endedwhenayoungmutantarrives,beingpursuedbydarkforces.";
    private static String yearMovie = "2017";


    @Rule
    public ActivityTestRule<DetailActivity> mDetailActivityTestRule =
            new ActivityTestRule<DetailActivity>(DetailActivity.class) {
                @Override
                protected Intent getActivityIntent() {
                    Context targetContext = InstrumentationRegistry.getInstrumentation()
                            .getTargetContext();
                    Intent result = new Intent(targetContext, MainActivity.class);
                    result.putExtra("movie", movieStr);
                    return result;
                }
            };


    @Test
    public void getMovie_showDetail() throws Exception {

        // Chechk views info showed
        onView(withId(R.id.tv_original_title_detail)).check(matches(withText(titleMovie)));

        onView(withId(R.id.tv_average_detail)).check(matches(withText(averageMovie)));

        onView(withId(R.id.tv_vote_count_detail)).check(matches(withText(voteCountMovie)));

        onView(withId(R.id.tv_overview_detail)).check(matches(withText(overviewMovie)));

        onView(withId(R.id.tv_year_detail)).check(matches(withText(yearMovie)));


        // Click on toolbar home button
        onView(isRoot()).perform(ViewActions.pressMenuKey());
    }

}